{-# LANGUAGE CPP                        #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeOperators              #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Proxy (Proxy(..))
import GHC.Generics (Generic)
import Miso
import Miso.String (concat, MisoString, ms)
import Prelude hiding (concat)
import Network.HTTP.Types.Method (Method)
import Network.URI (URI)
import Servant.API
import Servant.Client


#ifdef MIN_VERSION_servant

#if MIN_VERSION_servant(0,14,0)
import Servant.Links
#else
import Servant.Utils.Links
#endif

#if MIN_VERSION_servant(0,12,0)
#else
import qualified Data.ByteString.Lazy as B
import qualified Network.HTTP.Client as H
import qualified Network.HTTP.Types.Header as H
import qualified Network.HTTP.Media.MediaType as H
type Response = (Int, B.ByteString, H.MediaType, [H.Header], H.Response B.ByteString)
#endif

#endif


ghcVersion :: Int
ghcVersion =  __GLASGOW_HASKELL__

data Hero = Hero
    { heroName :: MisoString
    , heroImage :: MisoString
    } deriving (Eq, Generic, Show)

instance FromJSON Hero
instance ToJSON Hero

newtype Model = Model 
    { heroes_ :: [Hero]
    } deriving (Eq, Generic)

instance ToJSON Model

initialModel :: Model
initialModel = Model []

data Action
    = NoOp
    | SetHeroes [Hero]
    | FetchHeroes
    deriving (Eq, Show)

type ClientRoutes = HomeRoute

type HomeRoute = View Action

homeRoute :: Model -> View Action
homeRoute m = div_ 
    []
    [ h1_ [] [ text (ms $ "miso-test, GHC" ++ show ghcVersion) ]
    , ul_ [] (map fmtHero $ heroes_ m)
    , p_ [] [ a_ [href_ $ ms $ show linkHeroes] [ text "JSON data" ] ] 
    , p_ [] [ a_ [href_ $ ms $ show $ linkAdd 20 22 ] [ text "add 20 22" ] ] 
    , p_ [] [ a_ [href_ "https://gitlab.com/juliendehos/miso-test"] [ text "source code" ] ] 
    ]
    where fmtHero h = li_ [] 
            [ text $ heroName h
            , br_ []
            , img_ [ src_ $ concat [ms $ show linkStatic, "/", heroImage h] ]
            ]

type StaticApi = "static" :> Raw 
type AddApi = "add" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Int
type HeroesApi = "heroes" :>  Get '[JSON] [Hero] 
type PublicApi = StaticApi :<|> AddApi :<|> HeroesApi 

fetchStatic :: Method -> ClientM Response
fetchAdd :: Int -> Int -> ClientM Int
fetchHeroes :: ClientM [Hero]
fetchStatic :<|> fetchAdd :<|> fetchHeroes = client (Proxy :: Proxy PublicApi)

linkStatic :: URI
linkStatic = linkURI $ safeLink (Proxy :: Proxy PublicApi) (Proxy :: Proxy StaticApi)

linkAdd :: Int -> Int -> URI
linkAdd x y = linkURI (safeLink (Proxy :: Proxy PublicApi) (Proxy :: Proxy AddApi) x y)

linkHeroes :: URI
linkHeroes = linkURI $ safeLink (Proxy :: Proxy PublicApi) (Proxy :: Proxy HeroesApi)

