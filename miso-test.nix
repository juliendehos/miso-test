{ mkDerivation, aeson, base, binary, bytestring, http-client, http-media
, http-types, lucid, miso, network-uri, servant, servant-client
, servant-server , stdenv, wai, wai-extra, warp
, miso-action-logger ? ""
}:
mkDerivation {
  pname = "miso-test";
  version = "0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base binary bytestring http-client http-media http-types lucid miso
    miso-action-logger network-uri servant servant-client servant-server wai
    wai-extra warp
  ];
  license = stdenv.lib.licenses.publicDomain;
}
