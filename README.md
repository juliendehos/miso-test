
# Miso-test

A simple Miso example that can be built using different versions of ghc/ghcjs.  


## Build ghc/ghcjs

- check you have 20GB left (at least), on your hard drive
- run: `./init.sh`
- wait for 6h... (yes, really)


## Build and run the example

```
make clean
nix-shell -A client86 --run "make client"
nix-shell -A server86 --run "make server"

make clean
nix-shell -A client84 --run "make client"
nix-shell -A server84 --run "make server"

make clean
nix-shell -A client80 --run "make client"
nix-shell -A server80 --run "make server"

make clean
nix-shell -A client710 --run "make client"
nix-shell -A server710 --run "make server"
```

## TODO

- ghc/ghcjs 8.2 ?
- hydra ?
- docker ?

