let

  pkgs86 = import ./nix/nixpkgs.nix { ghc-version = "ghc864"; ghcjs-version = "ghcjs86"; };
  pkgs84 = import ./nix/nixpkgs.nix { ghc-version = "ghc844"; ghcjs-version = "ghcjs84"; };
  pkgs80 = import ./nix/nixpkgs80.nix;
  pkgs710 = import ./nix/nixpkgs710.nix;

in

  {

    server86 = (pkgs86.haskell.packages.ghc.callCabal2nix "miso-test" ./. {}).env;
    client86 = (pkgs86.haskell.packages.ghcjs.callCabal2nix "miso-test" ./. {}).env;

    server84 = (pkgs84.haskell.packages.ghc.callCabal2nix "miso-test" ./. {}).env;
    client84 = (pkgs84.haskell.packages.ghcjs.callCabal2nix "miso-test" ./. {}).env;

    server80 = (pkgs80.haskell.packages.ghc.callPackage ./miso-test.nix {}).env;
    client80 = (pkgs80.haskell.packages.ghcjs.callPackage ./miso-test.nix {}).env;

    server710 = (pkgs710.haskell.packages.ghc.callPackage ./miso-test.nix {}).env;
    client710 = (pkgs710.haskell.packages.ghcjs.callPackage ./miso-test.nix {}).env;

  }


