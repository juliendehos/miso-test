let

  bootstrap = import <nixpkgs> {};

  nixpkgs-src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    rev = "a0aeb23";
    sha256 = "04dgg0f2839c1kvlhc45hcksmjzr8a22q1bgfnrx71935ilxl33d";
  };

  miso-src = import ./miso.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };

  miso-action-logger-src = import ./miso-action-logger.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };

  config = { 
    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghc = pkgs.haskell.packages.ghc802.override {
            overrides = self: super: with pkgs.haskell.lib; {

              miso = self.callCabal2nix "miso" miso-src {};

              generics-sop = dontCheck (doJailbreak super.generics-sop);
              auto-update = dontCheck (doJailbreak super.auto-update);

            };
          };

          } // {

          ghcjs = pkgs.haskell.packages.ghcjsHEAD.override {
            overrides = self: super: with pkgs.haskell.lib; {

              appar = dontHaddock super.appar;
              auto-update = dontHaddock super.auto-update;
              base-orphans = dontHaddock super.base-orphans;
              byteable = dontHaddock super.byteable;
              byteorder = dontHaddock super.byteorder;
              bytestring-builder = dontHaddock super.bytestring-builder;
              hscolour = dontHaddock super.hscolour;
              file-embed = dontHaddock super.file-embed;

              miso = self.callCabal2nix "miso" miso-src {};

              miso-action-logger = self.callCabal2nix "miso-action-logger" miso-action-logger-src {};

            };
          };

        };
      };
    };
  };

in

  import nixpkgs-src { inherit config; }

