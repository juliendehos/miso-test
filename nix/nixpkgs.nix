{ ghc-version, ghcjs-version }:

let

  bootstrap = import <nixpkgs> {};

  nixpkgs-src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    rev = "725b5499b89fe80d7cfbb00bd3c140a73cbdd97f";
    sha256 = "0xdhv9k0nq8d91qdw66d6ln2jsqc9ij7r24l9jnv4c4bfpl4ayy7";
  };

  miso-src = import ./miso.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };

  miso-action-logger-src = import ./miso-action-logger.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };

  config = { 
    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghc = pkgs.haskell.packages.${ghc-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              miso = self.callCabal2nix "miso" miso-src {};

            };
          };

          } // {

          ghcjs = pkgs.haskell.packages.${ghcjs-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              tasty-quickcheck = dontCheck super.tasty-quickcheck;
              http-types       = dontCheck super.http-types;
              comonad          = dontCheck super.comonad;
              semigroupoids    = dontCheck super.semigroupoids;
              lens             = dontCheck super.lens;
              QuickCheck       = dontCheck super.QuickCheck;

              network = dontCheck (doJailbreak super.network_2_6_3_1);
              servant-client = dontCheck (doJailbreak super.servant-client);
              servant = dontCheck (doJailbreak super.servant);

              miso = self.callCabal2nix "miso" miso-src {};

              miso-action-logger = self.callCabal2nix "miso-action-logger" miso-action-logger-src {};

            };
          };

        };
      };
    };
  };

in

  import nixpkgs-src { inherit config; }

