{ fetchFromGitHub }:

fetchFromGitHub {
  owner = "Lermex";
  repo = "miso-action-logger";
  sha256 = "1q6bpckz355paxcs10223fnl1d3lnxz2vcgj8l7nqvclicp04hsb";
  rev = "917af7edc33e86b1510ebf2bc8e1b1bb9f03c164";
}

