{ fetchFromGitHub }:

fetchFromGitHub {

    owner = "juliendehos";
    repo = "miso";
    rev = "f55e127bc7f35b98534896abd81b02167976f33b";
    sha256 = "1h46mmlcszm4nmwq7yiy3sdks0xy3ww3pm41cz6zpg1hmikf40fm";

    # owner = "haskell-miso";
    # repo = "miso";
    # rev = "0.21.2.0";
    # sha256 = "07k1rlvl9g027fp2khl9kiwla4rcn9sv8v2dzm0rzf149aal93vn";

  }

