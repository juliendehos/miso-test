let

  bootstrap = import <nixpkgs> {};

  nixpkgs-src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    rev = "a0aeb23";
    sha256 = "04dgg0f2839c1kvlhc45hcksmjzr8a22q1bgfnrx71935ilxl33d";
  };

  generic-sop-src = fetchTarball "https://github.com/well-typed/generics-sop/archive/generics-sop-0.3.2.0.tar.gz";
  lucid-src = fetchTarball "https://github.com/chrisdone/lucid/archive/v2.9.11.tar.gz";

  miso-src = import ./miso.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };
  # miso-src = /home/dehos/depots/github/juliendehos/miso;

  miso-action-logger-src = import ./miso-action-logger.nix { fetchFromGitHub = bootstrap.fetchFromGitHub; };

  config = { 
    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghc = pkgs.haskell.packages.ghc7103.override {
            overrides = self: super: with pkgs.haskell.lib; {

              servant = dontCheck super.servant;

              miso = self.callCabal2nix "miso" miso-src {};

              generics-sop = self.callCabal2nix "generics-sop" generic-sop-src {};
              lucid = self.callCabal2nix "lucid" lucid-src {};

            };
          };

          } // {

          ghcjs = pkgs.haskell.packages.ghcjs.override {
            overrides = self: super: with pkgs.haskell.lib; {

              appar = dontHaddock super.appar;
              auto-update = dontHaddock super.auto-update;
              base-orphans = dontHaddock super.base-orphans;
              byteable = dontHaddock super.byteable;
              byteorder = dontHaddock super.byteorder;
              bytestring-builder = dontHaddock super.bytestring-builder;
              hscolour = dontHaddock super.hscolour;
              file-embed = dontHaddock super.file-embed;

              servant = dontCheck super.servant;

              miso = self.callCabal2nix "miso" miso-src {};

              miso-action-logger = self.callCabal2nix "miso-action-logger" miso-action-logger-src {};

              generics-sop = self.callCabal2nix "generics-sop" generic-sop-src {};
              lucid = self.callCabal2nix "lucid" lucid-src {};

            };
          };

        };
      };
    };
  };

in

  import nixpkgs-src { inherit config; }

